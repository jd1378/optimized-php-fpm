# alpine nginx unit with php

an alpine nginx unit with php (8.1 currently) and extensions needed to run laravel apps

## building locally

```sh
docker build --tag nginx-unit-alpine-php .
```

## Notes

check Dockerfile to see which modules are installed.
laravel projects should work out of box.
`www-data` user is created and added to the group with the same name for you.

### IMPORTANT

Don't use below examples as is, they don't take care of caching, environment variables etc. Make sure you know what you are doing.

## How to use

1. put your config in a file like `.unit.config.json`
2. copy `.unit.config.json` to `/docker-entrypoint.d/` directory

Example:

```Dockerfile

FROM javadmnjd/nginx-unit-alpine-php

COPY . /var/www/

RUN mkdir -p /var/www/storage/logs/ \
    && chown -R www-data:www-data /var/www \
    && mv /var/www/unit.config.json /docker-entrypoint.d/

WORKDIR /var/www/

CMD ["/usr/sbin/unitd", "--no-daemon", "--control","unix:/var/run/control.unit.sock"]

EXPOSE 80

```

Example `.unit.config.json` for the above Dockerfile:

```json
{
  "listeners": {
    "*:80": {
      "pass": "routes"
    }
  },
  "routes": [
    {
      "match": {
        "uri": [
          "*.manifest",
          "*.appcache",
          "*.html",
          "*.json",
          "*.rss",
          "*.atom",
          "*.jpg",
          "*.jpeg",
          "*.gif",
          "*.png",
          "*.ico",
          "*.cur",
          "*.gz",
          "*.svg",
          "*.svgz",
          "*.mp4",
          "*.ogg",
          "*.ogv",
          "*.webm",
          "*.htc",
          "*.css",
          "*.js",
          "*.ttf",
          "*.ttc",
          "*.otf",
          "*.eot",
          "*.woff",
          "*.woff2",
          "*.txt"
        ]
      },
      "action": {
        "share": "/var/www/public$uri"
      }
    },
    {
      "action": {
        "pass": "applications/php"
      }
    }
  ],
  "applications": {
    "php": {
      "type": "php",
      "limits": {
        "requests": 1000,
        "timeout": 60
      },
      "processes": {
        "max": 16,
        "spare": 4,
        "idle_timeout": 30
      },
      "user": "www-data",
      "group": "www-data",
      "working_directory": "/var/www/",
      "root": "/var/www/public",
      "script": "index.php",
      "index": "index.php"
    }
  },
  "access_log": "/dev/stdout"
}
```
